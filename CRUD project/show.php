<?php include('showserver.php'); 

	if(isset($_GET['edit']))
	{
	$NO=$_GET['edit'];
	$edit_state=true;
	
	$rec=mysqli_query($db,"SELECT * FROM companydata WHERE NO=$NO");
	$record=mysqli_fetch_array($rec);
	$CompanyName=$record['CompanyName'];
	$Address=$record['Address'];
	$PhoneNumber=$record['PhoneNumber'];
	$Email=$record['Email'];
	$NO=$record['NO'];
}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Show and CRUD</title>
	<link rel="stylesheet" type="text/css" href="showstyle.css">
</head>
<body>

	<?php if(isset($_SESSION['msg'])):?>
		<div class="msg">
			<?php
				echo $_SESSION['msg'];
				unset($_SESSION['msg']);
			?>
		</div>
	<?php endif ?>
	<table>
		<thead>
			<tr>
				<th>NO</th>
				<th>CompanyName</th>
				<th>Address</th>
				<th>PhoneNumber</th>
				<th>Email</th>
				<th colspan="2">Action</th>
			</tr>
		</thead>
		<tbody>
			<?php while($row=mysqli_fetch_array($results)){?>
			<tr>
				<td> <?php echo $row['NO']; ?> </td>
				<td> <?php echo $row['CompanyName']; ?> </td>
				<td> <?php echo $row['Address']; ?> </td>
				<td> <?php echo $row['PhoneNumber']; ?> </td>
				<td> <?php echo $row['Email']; ?> </td>
				<td><a class="edit_btn" href="show.php?edit=<?php echo $row['NO']; ?>">Edit</td>
				<td><a class="del_btn" href="showserver.php?del=<?php echo $row['NO']; ?>">Delete</td>
			</tr> 
			<?php } ?>
		</tbody>
	</table>
	
	<form method="post" action="showserver.php">
	<input type="hidden" name="NO" value="<?php echo $NO; ?>">
		<div class="input-group">
			<label>CompanyName</label>
			<input type="text" name="CompanyName" value="<?php echo $CompanyName; ?>">
		</div>
		<div class="input-group">
			<label>Address</label>
			<input type="text" name="Address" value="<?php echo $Address; ?>">
		</div>
		<div class="input-group">
			<label>PhoneNumber</label>
			<input type="text" name="PhoneNumber" value="<?php echo $PhoneNumber; ?>">
		</div>	
		<div class="input-group">
			<label>Email</label>
			<input type="text" name="Email" value="<?php echo $Email; ?>">
		</div>	
		<div class="input-group">
		<?php if ($edit_state==false): ?>
			<button type="submit" name="save" class="btn">SAVE</button>
		<?php else: ?>
			<button type="submit" name="update" class="btn">UPDATE</button>
		<?php endif ?>
		</div>
	</form>
	
	
	
</body>
</html>